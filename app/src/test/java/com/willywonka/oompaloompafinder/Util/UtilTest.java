package com.willywonka.oompaloompafinder.Util;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by pc on 03/04/2018.
 */
public class UtilTest {
    @Test
    public void uppercaseFirstLetterFromString1() throws Exception {
        Assert.assertEquals("Test", Util.uppercaseFirstLetterFromString("test"));
    }
    @Test
    public void uppercaseFirstLetterFromString2() throws Exception {
        Assert.assertEquals("Oompa", Util.uppercaseFirstLetterFromString("oompa"));
    }
    @Test
    public void uppercaseFirstLetterFromString3() throws Exception {
        Assert.assertEquals("Loompa", Util.uppercaseFirstLetterFromString("loompa"));
    }
    @Test
    public void uppercaseFirstLetterFromStringFail() throws Exception {
        Assert.assertEquals("test", Util.uppercaseFirstLetterFromString("test"));
    }
}