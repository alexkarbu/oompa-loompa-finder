package com.willywonka.oompaloompafinder.Activities;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;

import com.willywonka.oompaloompafinder.R;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withSpinnerText;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.core.IsAnything.anything;


/**
 * Created by pc on 03/04/2018.
 */
public class MainActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule =
            new ActivityTestRule<>(MainActivity.class);

    @Before
    public void init() {
        mActivityRule.getActivity()
                .getSupportFragmentManager().beginTransaction();
    }

    @Test
    public void ensureTextViewFavoriteColorWork1() throws Exception {
        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.textViewDetailsFavoriteColor))
                .check(matches(withText("Red")));
    }

    @Test
    public void ensureTextViewFavoriteColorWork2() throws Exception {
        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));
        onView(withId(R.id.textViewDetailsFavoriteColor))
                .check(matches(withText("Blue")));
    }

    @Test
    public void ensureTextViewFavoriteFoodWork1() throws Exception {
        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.textViewDetailsFavoriteFood))
                .check(matches(withText("Chocolat")));
    }

    @Test
    public void ensureTextViewFavoriteFoodWork2() throws Exception {
        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(3, click()));
        onView(withId(R.id.textViewDetailsFavoriteFood))
                .check(matches(withText("Cocoa nuts")));
    }

    @Test
    public void ensureTheGenderFilterWorks1() throws Exception  {
        onView(withId(R.id.spinnerListFilterGender)).perform(click());
        onData(anything()).atPosition(1).perform(click());
        onView(withId(R.id.spinnerListFilterGender)).check(matches(withSpinnerText(containsString("Male"))));

        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.textViewDetailsGender))
                .check(matches(withText("M")));
    }

    @Test
    public void ensureTheGenderFilterWorks2() throws Exception  {
        onView(withId(R.id.spinnerListFilterGender)).perform(click());
        onData(anything()).atPosition(2).perform(click());
        onView(withId(R.id.spinnerListFilterGender)).check(matches(withSpinnerText(containsString("Female"))));

        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.textViewDetailsGender))
                .check(matches(withText("F")));
    }

    @Test
    public void ensureTheProfessionFilterWorks1() throws Exception  {
        onView(withId(R.id.spinnerListFilterProfession)).perform(click());
        onData(anything()).atPosition(1).perform(click());
        onView(withId(R.id.spinnerListFilterProfession)).check(matches(withSpinnerText(containsString("Developer"))));

        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.textViewDetailsProfession))
                .check(matches(withText("Working as Developer")));
    }


    @Test
    public void ensureTheProfessionFilterWorks2() throws Exception  {
        onView(withId(R.id.spinnerListFilterProfession)).perform(click());
        onData(anything()).atPosition(2).perform(click());
        onView(withId(R.id.spinnerListFilterProfession)).check(matches(withSpinnerText(containsString("Metalworker"))));

        onView(withId(R.id.recyclerViewList))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.textViewDetailsProfession))
                .check(matches(withText("Working as Metalworker")));
    }
}