package com.willywonka.oompaloompafinder.Interfaces;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alex on 29/03/2018.
 */

public class RetrofitInterface {
    public static final String BASE_URL = "https://2q2woep105.execute-api.eu-west-1.amazonaws.com/napptilus/";
    private static Retrofit retrofit = null;

    public static Retrofit getInterface(){
        if (retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
