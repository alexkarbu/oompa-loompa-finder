package com.willywonka.oompaloompafinder.Interfaces.Services;

import com.willywonka.oompaloompafinder.Models.OompaLoompa;
import com.willywonka.oompaloompafinder.Models.OompaLoompaList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Alex on 29/03/2018.
 */

public interface RetrofitService {
    @GET("oompa-loompas/{id}")
    Call<OompaLoompa> getOompaLoompa(@Path("id") int id);

    @GET("oompa-loompas")
    Call<OompaLoompaList> getOompaLoompaList();
}
