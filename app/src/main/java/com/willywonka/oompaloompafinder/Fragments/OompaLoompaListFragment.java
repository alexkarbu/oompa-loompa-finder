package com.willywonka.oompaloompafinder.Fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.willywonka.oompaloompafinder.Adapters.MyOompaLoompaRecyclerViewAdapter;
import com.willywonka.oompaloompafinder.Interfaces.RetrofitInterface;
import com.willywonka.oompaloompafinder.Interfaces.Services.RetrofitService;
import com.willywonka.oompaloompafinder.Models.OompaLoompa;
import com.willywonka.oompaloompafinder.Models.OompaLoompaList;
import com.willywonka.oompaloompafinder.R;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OompaLoompaListFragment extends Fragment {

    private OnListFragmentInteractionListener mListener;
    private RecyclerView recyclerView;
    private MyOompaLoompaRecyclerViewAdapter recyclerViewAdapter;
    private Spinner spinnerListFilterGender, spinnerListFilterProfession;

    private OompaLoompaList oompaLoompaList;
    private Context context;

    private RelativeLayout relativeLayoutList;
    private ProgressBar progressBarList;

    private String gender, profession;

    private SwipeRefreshLayout swipeRefreshLayoutList;

    RetrofitService service;

    private View view;

    public OompaLoompaListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_oompaloompa_list, container, false);

        context = container.getContext();

        bindUI();

        service = RetrofitInterface.getInterface().create(RetrofitService.class);
        loadOompaLoompaList(service);

        gender = "All";
        profession = "All";

        swipeRefreshLayoutList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                recyclerView.setVisibility(View.INVISIBLE);
                loadOompaLoompaList(service);
            }
        });

        return view;
    }

    private void bindUI() {
        recyclerView = view.findViewById(R.id.recyclerViewList);
        spinnerListFilterGender = view.findViewById(R.id.spinnerListFilterGender);
        spinnerListFilterProfession = view.findViewById(R.id.spinnerListFilterProfession);
        relativeLayoutList = view.findViewById(R.id.relativeLayoutList);
        progressBarList = view.findViewById(R.id.progressBarList);
        swipeRefreshLayoutList = view.findViewById(R.id.swipeRefreshLayoutListFragment);
        recyclerView.setLayoutManager(new GridLayoutManager(context, 2));
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(OompaLoompa item);
    }

    private void loadOompaLoompaList(RetrofitService service) {
        Call<OompaLoompaList> oompaLoompaListCall = service.getOompaLoompaList();
        oompaLoompaListCall.enqueue(new Callback<OompaLoompaList>() {
            @Override
            public void onResponse(Call<OompaLoompaList> call, Response<OompaLoompaList> response) {
                //Data & RecyclerView
                oompaLoompaList = response.body();
                recyclerViewAdapter = new MyOompaLoompaRecyclerViewAdapter(context, oompaLoompaList.getOompaLoompas(), mListener);
                recyclerView.setAdapter(recyclerViewAdapter);

                //Spinners
                setUpSpinnerGender();
                setUpSpinnerProfession();

                //Visibility
                progressBarList.setVisibility(View.GONE);
                relativeLayoutList.setVisibility(View.VISIBLE);
                recyclerView.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Call<OompaLoompaList> call, Throwable t) {
                Toast.makeText(context, context.getString(R.string.network_error), Toast.LENGTH_SHORT);
            }
        });
    }

    private void setUpSpinnerProfession() {
        List<String> professionArray = oompaLoompaList.getProfessions();
        professionArray.add(0, context.getResources().getString(R.string.all));

        ArrayAdapter<String> professionSpinnerAdapter = new ArrayAdapter<>(context,
                android.R.layout.simple_spinner_item, professionArray);
        professionSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerListFilterProfession.setAdapter(professionSpinnerAdapter);
        spinnerListFilterProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                profession = selectedItem;
                String filteringOptions = gender + "-" + profession;
                recyclerViewAdapter.getFilter().filter(filteringOptions);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setUpSpinnerGender() {
        //Gender Spinner Filter
        String[] genderArray = {context.getResources().getString(R.string.all), context.getResources().getString(R.string.male), context.getResources().getString(R.string.female)};
        ArrayAdapter<String> genderSpinnerAdapter = new ArrayAdapter<>(context, android.R.layout.simple_spinner_item, genderArray);
        genderSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerListFilterGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                if (selectedItem.equals(context.getResources().getString(R.string.male))) {
                    gender = "M";
                } else if (selectedItem.equals(context.getResources().getString(R.string.female))) {
                    gender = "F";
                } else {
                    gender = "All";
                }
                String filteringOptions = gender + "-" + profession;
                recyclerViewAdapter.getFilter().filter(filteringOptions);
                swipeRefreshLayoutList.setRefreshing(false);
            }

            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
        spinnerListFilterGender.setAdapter(genderSpinnerAdapter);
    }
}
