package com.willywonka.oompaloompafinder.Fragments;


import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.willywonka.oompaloompafinder.Interfaces.RetrofitInterface;
import com.willywonka.oompaloompafinder.Interfaces.Services.RetrofitService;
import com.willywonka.oompaloompafinder.Models.Favorite;
import com.willywonka.oompaloompafinder.Models.OompaLoompa;
import com.willywonka.oompaloompafinder.R;
import com.willywonka.oompaloompafinder.Util.Util;

import org.jsoup.Jsoup;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OompaLoompaDetailsFragment extends Fragment {

    private TextView textViewDetailsName, textViewDetailsProfession, textViewDetailsGender, textViewDetailsAge, textViewDetailsDescription, textViewDetailsQuota;
    private TextView textViewDetailsHeight, textViewDetailsCountry, textViewDetailsEmail, textViewDetailsFavoriteFood, textViewDetailsFavoriteSong, textViewDetailsFavoriteColor;
    private ImageView imageViewDetailsProfile, imageViewDetailsDescriptionIcon, imageViewDetailsFavoriteSongIcon;

    private ScrollView layoutOompaLoompaDetails;
    private CardView cardViewDetailsDescription, cardViewDetailsFavoriteSong;
    private ProgressBar progressBarDetails;

    private int id;
    private View view;
    private Context context;


    public OompaLoompaDetailsFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            id = getArguments().getInt("id");
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_oompa_loompa_details, container, false);

        context = container.getContext();

        bindUI();

        RetrofitService service = RetrofitInterface.getInterface().create(RetrofitService.class);
        loadOompaLoompa(service, id);

        return view;
    }

    private void bindUI() {
        layoutOompaLoompaDetails = view.findViewById(R.id.layoutOompaLoompaDetails);
        cardViewDetailsDescription = view.findViewById(R.id.cardViewDetailsDescription);
        cardViewDetailsFavoriteSong = view.findViewById(R.id.cardViewDetailsFavoriteSong);
        progressBarDetails = view.findViewById(R.id.progressBarDetails);

        textViewDetailsName = view.findViewById(R.id.textViewDetailsName);
        imageViewDetailsProfile = view.findViewById(R.id.imageViewDetailsProfile);
        textViewDetailsAge = view.findViewById(R.id.textViewDetailsAge);
        textViewDetailsProfession = view.findViewById(R.id.textViewDetailsProfession);
        textViewDetailsGender = view.findViewById(R.id.textViewDetailsGender);
        textViewDetailsDescription = view.findViewById(R.id.textViewDetailsDescription);
        imageViewDetailsDescriptionIcon = view.findViewById(R.id.imageViewDetailsDescriptionIcon);
        textViewDetailsQuota = view.findViewById(R.id.textViewDetailsQuota);
        textViewDetailsHeight = view.findViewById(R.id.textViewDetailsHeight);
        textViewDetailsCountry = view.findViewById(R.id.textViewDetailsCountry);
        textViewDetailsEmail = view.findViewById(R.id.textViewDetailsEmail);

        textViewDetailsFavoriteFood = view.findViewById(R.id.textViewDetailsFavoriteFood);
        textViewDetailsFavoriteSong = view.findViewById(R.id.textViewDetailsFavoriteSong);
        imageViewDetailsFavoriteSongIcon = view.findViewById(R.id.imageViewDetailsFavoriteSongIcon);
        textViewDetailsFavoriteColor = view.findViewById(R.id.textViewDetailsFavoriteColor);
    }

    private void loadOompaLoompa(RetrofitService service, int id) {
        Call<OompaLoompa> oompaLoompaCall = service.getOompaLoompa(id);
        oompaLoompaCall.enqueue(new Callback<OompaLoompa>() {
            @Override
            public void onResponse(Call<OompaLoompa> call, Response<OompaLoompa> response) {
                OompaLoompa oompaLoompa = response.body();
                renderProfile(oompaLoompa);
            }

            @Override
            public void onFailure(Call<OompaLoompa> call, Throwable t) {
                //Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_SHORT);
            }
        });
    }

    private void renderProfile(OompaLoompa profile) {
        //Image
        Glide.with(this)
                .load(profile.getImage())
                .into(imageViewDetailsProfile);
        textViewDetailsName.setText(profile.getFirstName() + " " + profile.getLastName());

        //Profile
        textViewDetailsProfession.setText(context.getString(R.string.working_as) + " " + profile.getProfession());
        textViewDetailsGender.setText(profile.getGender());
        textViewDetailsAge.setText("" + profile.getAge());
        textViewDetailsDescription.setText(Jsoup.parse(profile.getDescription()).text());
        cardViewDetailsDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textViewDetailsDescription.getVisibility() == View.VISIBLE) {
                    textViewDetailsDescription.setVisibility(View.GONE);
                    imageViewDetailsDescriptionIcon.setImageResource(R.mipmap.ic_more);
                } else {
                    textViewDetailsDescription.setVisibility(View.VISIBLE);
                    imageViewDetailsDescriptionIcon.setImageResource(R.mipmap.ic_less);

                }
            }
        });
        //textViewDetailsQuota.setText(profile.getQuota());
        textViewDetailsHeight.setText("" + profile.getHeight() + " " + context.getString(R.string.cm));
        textViewDetailsCountry.setText(context.getString(R.string.living_in) + " " + profile.getCountry());
        textViewDetailsEmail.setText(profile.getEmail());

        //Favorite
        Favorite favorite = profile.getFavorite();
        textViewDetailsFavoriteFood.setText(Util.uppercaseFirstLetterFromString(favorite.getFood()));
        textViewDetailsFavoriteColor.setText(Util.uppercaseFirstLetterFromString(favorite.getColor()));
        textViewDetailsFavoriteSong.setText(Util.uppercaseFirstLetterFromString(favorite.getSong()));

        cardViewDetailsFavoriteSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (textViewDetailsFavoriteSong.getVisibility() == View.VISIBLE) {
                    textViewDetailsFavoriteSong.setVisibility(View.GONE);
                    imageViewDetailsFavoriteSongIcon.setImageResource(R.mipmap.ic_more);
                } else {
                    textViewDetailsFavoriteSong.setVisibility(View.VISIBLE);
                    imageViewDetailsFavoriteSongIcon.setImageResource(R.mipmap.ic_less);
                }
            }
        });

        //Layout
        progressBarDetails.setVisibility(View.GONE);
        layoutOompaLoompaDetails.setVisibility(View.VISIBLE);
    }


}
