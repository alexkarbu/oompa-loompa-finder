package com.willywonka.oompaloompafinder.Util;

/**
 * Created by pc on 01/04/2018.
 */

public class Util {
    public static String uppercaseFirstLetterFromString(String string) {
        String upperString = string.substring(0, 1).toUpperCase() + string.substring(1);
        return upperString;
    }
}
