package com.willywonka.oompaloompafinder.Activities;


import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.willywonka.oompaloompafinder.Fragments.OompaLoompaDetailsFragment;
import com.willywonka.oompaloompafinder.Fragments.OompaLoompaListFragment;
import com.willywonka.oompaloompafinder.Models.OompaLoompa;
import com.willywonka.oompaloompafinder.R;

public class MainActivity extends AppCompatActivity implements OompaLoompaListFragment.OnListFragmentInteractionListener {

    FragmentManager fragmentManager;

    Fragment listFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        listFragment = new OompaLoompaListFragment();
        fragmentTransaction.replace(R.id.frameLayout, listFragment);
        fragmentTransaction.commit();
    }


    @Override
    public void onListFragmentInteraction(OompaLoompa item) {
        Bundle data = new Bundle();
        data.putInt("id", item.getId());

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        Fragment newFragment = new OompaLoompaDetailsFragment();
        newFragment.setArguments(data);
        fragmentTransaction.hide(listFragment);
        fragmentTransaction.add(R.id.frameLayout, newFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
