package com.willywonka.oompaloompafinder.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.willywonka.oompaloompafinder.Fragments.OompaLoompaListFragment.OnListFragmentInteractionListener;
import com.willywonka.oompaloompafinder.Models.OompaLoompa;
import com.willywonka.oompaloompafinder.R;

import java.util.ArrayList;
import java.util.List;

public class MyOompaLoompaRecyclerViewAdapter extends RecyclerView.Adapter<MyOompaLoompaRecyclerViewAdapter.ViewHolder> implements Filterable {

    private final List<OompaLoompa> mValues;
    private List<OompaLoompa> mFilteredList;
    private final OnListFragmentInteractionListener mListener;
    private Context context;

    public MyOompaLoompaRecyclerViewAdapter(Context context, List<OompaLoompa> oompaLoompas, OnListFragmentInteractionListener listener) {
        mValues = oompaLoompas;
        mFilteredList = mValues;
        mListener = listener;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recyclerview_oompaloompa_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mOompaLoompa = mFilteredList.get(position);
        holder.mBasicInfo.setText(mFilteredList.get(position).getLastName() + ", " + mFilteredList.get(position).getFirstName() + " (" + mFilteredList.get(position).getAge() + (")"));
        holder.mSecondaryInfo.setText(mFilteredList.get(position).getCountry());
        holder.mProfession.setText(mFilteredList.get(position).getProfession());
        //holder.mGender.setText(mValues.get(position).getGender());

        Glide.with(context)
                .load(mFilteredList.get(position).getImage())
                .into(holder.mImage);

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {

                    mListener.onListFragmentInteraction(holder.mOompaLoompa);

                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String[] splitFilterValue = charSequence.toString().split("-");
                String gender = splitFilterValue[0];
                String profession = splitFilterValue[1];

                List<OompaLoompa> filteredList = new ArrayList<>();

                if ((gender.isEmpty() || gender.equals("All")) && (profession.isEmpty() || profession.equals("All"))) {
                    mFilteredList = mValues;
                } else if ((profession.isEmpty() || profession.equals("All"))) {
                    for (OompaLoompa oompaLoompa : mValues) {
                        if (oompaLoompa.getGender().contains(gender)) {
                            filteredList.add(oompaLoompa);
                        }
                    }
                    mFilteredList = filteredList;
                } else if ((gender.isEmpty() || gender.equals("All"))) {
                    for (OompaLoompa oompaLoompa : mValues) {
                        if (oompaLoompa.getProfession().contains(profession)) {
                            filteredList.add(oompaLoompa);
                        }
                    }
                    mFilteredList = filteredList;
                } else {
                    for (OompaLoompa oompaLoompa : mValues) {
                        if (oompaLoompa.getProfession().contains(profession) && oompaLoompa.getGender().contains(gender)) {
                            filteredList.add(oompaLoompa);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (List<OompaLoompa>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mBasicInfo;
        public final TextView mSecondaryInfo;
        public final ImageView mImage;
        public final TextView mProfession;
        public final TextView mGender;

        public OompaLoompa mOompaLoompa;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mBasicInfo = view.findViewById(R.id.textViewBasicInfo);
            mSecondaryInfo = view.findViewById(R.id.textViewSecondaryInfo);
            mImage = view.findViewById(R.id.imageViewProfile);
            mProfession = view.findViewById(R.id.textViewProfession);
            mGender = view.findViewById(R.id.textViewGender);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mSecondaryInfo.getText() + "'";
        }
    }
}
