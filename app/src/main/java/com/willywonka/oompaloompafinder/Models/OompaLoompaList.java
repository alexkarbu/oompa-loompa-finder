package com.willywonka.oompaloompafinder.Models;

/**
 * Created by Alex on 29/03/2018.
 */

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class OompaLoompaList {

    @SerializedName("results")
    private List<OompaLoompa> oompaLoompas = null;

    public List<OompaLoompa> getOompaLoompas() {
        return oompaLoompas;
    }

    public List<String> getProfessions() {
        List<String> professions = new ArrayList<>();
        for (OompaLoompa oompaLoompa : oompaLoompas) {
            String profession = oompaLoompa.getProfession();
            if (!professions.contains(profession)) {
                professions.add(profession);
            }
        }
        return professions;
    }
}

