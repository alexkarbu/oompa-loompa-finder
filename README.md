# README #

App created by Alex Carbonell.

### Dependencies usage ###

-Retrofit has been used to download all the data from the server. There are other options available like Volley but I decided to use this one because I wanted to try a new one. And it really surprised me, this library is very powerful, the code looks cleaner and is pretty easy to use. My favorite from today. I've made and interface that is called when the application needs to download data. It has two functions: a list GET and the individual Oompa Loompa GET that needs a single parameter (id). 

-Gson has been used to parse all the Json data and convert it to Java Objects. This library makes the parsing process really easy and simple. Gson parses automatically and converts all the data into an Object List. Gson is added to the Retrofit interface directly, it just needs to declare the type of the object in the Retrofit Call and it converts everything automatically.

-Glide has been used to load all the images of the application. The usage is simple and easy. It's similar to Picasso. Glide has some cool features like Gif support. In this project it had a really basic purpose, Picasso could be used too but I decided to give Glide a try. Glide is loading the images from the URLs given, it can resize, crop and more with just some parameters.

-Jsoup has been used to parse Html code and maintain the original text. There are some HTML tags in the "description" field of some Oompa Loompas. Jsoup has a parsing function that deletes all the HTML tags and it maintains the entire string. 

-Espresso has been used to implement instrumented tests. I didn't know the existence of this library but when I wanted to realize some instrumented tests I found this one. It's used to test the UI. There are more options, for example: Robotium. I decided to use this one because it was already integrated in Android Studio. 